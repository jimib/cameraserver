/* global _config */
var express = require("express"),
	path = require("path"),
	fs = require("fs"),
	request = require("request");

var app = module.exports = express();
app.set( "views", path.join( __dirname, "views" ) );

var urlImages = path.join( "images" )
var images = [];

function imageFormats( imageName ){
	return {
		url : path.join( urlImages, imageName ),
		sizes : {
			full : path.join( urlImages, imageName ),
			medium : path.join( urlImages, "400", imageName ),
			small : path.join( urlImages, "200", imageName ),
			thumb : path.join( urlImages, "100", imageName )
		}
	}
}

app.use( function( req, res, next ){
	res.result = function( err, data ){
		res.send({
			status : err ? "error" : "ok",
			error : err,
			data : data
		});
	}
	next();
});

app.get("/", function( req, res, next ){
	res.render("index");
});

app.get("/images", function( req, res, next ){
	res.result(null,images);
});

app.get("/images/:sizeImage/:nameImage", function( req, res, next ){
	var nameImage = req.params.nameImage;
	var sizeImage = Number( req.params.sizeImage );
	
	console.log("requested image");
	if( !isNaN( sizeImage ) ){
		var dirImageResized = path.join( __dirname, "public", urlImages, sizeImage.toString());
		var pathImageResized = path.join( dirImageResized, nameImage );
		
		fs.exists(pathImageResized, function( boolExists ){
			if( boolExists ){
				res.sendFile( pathImageResized );
			}else{
				var pathImageFull = path.join( __dirname, "public", urlImages, nameImage );
				fs.exists(pathImageFull, function( boolExists ){
					if( boolExists ){
						//resize it
						console.log("loading image");
						require('lwip').open( pathImageFull, function(err, image){
							// check err...
							if( err )return next(err);
							console.log( image );
							var scale = sizeImage / Math.max( image.width(), image.height() );
							
							if( scale >= 1 ){
								res.sendFile( pathImageFull );
							}else{
								// define a batch of manipulations and save to disk as JPEG:
								//ensure the directory exists
								console.log("prepare image");
								fs.mkdir(dirImageResized, function(){
									console.log("resizing image");
									image.batch()
									.scale( scale )
									.writeFile(pathImageResized, function(err){
										if( err ){
											next( err );
										}else{
											console.log("completed image");
											res.sendFile( pathImageResized );
										}
									});
								});
							}
							
						});
					}else{
						//no full sized image - skip
						next();
					}	
				})
			}
		});
	}else{
		//incorrect format - skip
		next();
	}
	
});

app.post("/images", function( req, res, next ){
	var nameOutput =  new Date().getTime().toString() + ".jpg"
	var pathOutput = path.join( __dirname, "public", urlImages, nameOutput );
	var fileOutput = fs.createWriteStream( pathOutput );
	//generates an image
	console.log("image requested");
	request( _config.serverCamera ).on("end", function(){
		var image = imageFormats( nameOutput );
		images.push( image );
		console.log("image captured");
		res.result( null, image );
	})
	.on("error", res.result )
	.pipe( fileOutput );
});

//set up our css render engine
var pathCSS = path.join( __dirname, "public", "css" )
app.use( "/css", require("stylus").middleware({
	src: pathCSS,
	dest: pathCSS,
	debug: true,
	force: true,
	sourcemap:{
		inline:true
	}
}));

app.use( express.static( path.join( __dirname, "public" ) ) );