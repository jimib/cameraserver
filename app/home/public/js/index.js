$( document ).ready( function(){
	console.log("document is ready");
	var modelApp = new AppModel();
	
	window.showLoader = function(){
		modelApp.isLoading( true );
	};
	
	window.hideLoader = function(){
		modelApp.isLoading( false );
	};
	
	modelApp.captureImage = function(){
		console.log("capture image");
		showLoader();
		$.post("/images",{},function( result ){
			if( result && result.status == "ok" ){
				var image = result.data;
				modelApp.imageSelected( image );
				modelApp.images.push( image );
			}
			hideLoader();
		},'json');
	}
	
	ko.applyBindings( modelApp );
	
	setTimeout( function(){
		$.getJSON("/images", function( result ){
			if( result && result.status == "ok" ){
				modelApp.images( result.data );
			}
		});
	}, 10 );
	
} );

function AppModel(){
	var self = this;
	
	self.images = ko.observableArray([]);
	self.imageSelected = ko.observable();
	
	var numItemsLoading = ko.observable( 0 );
	self.isLoading = ko.computed({
		read : function(){
			return numItemsLoading() > 0 ? true : false;
		},
		write : function( val ){
			numItemsLoading( numItemsLoading() + ( val ? 1 : -1 ) );
		}
	});
}