/* global paper */
ko.components.register("gallery",{
	viewModel : GalleryModel,
	template : {element:"component-gallery"}
});

function GalleryModel( params ){
	var self = this;
	
	params = params || {};
	
	console.log( params );
	self.images = params.images || ko.observableArray([]);
}

$.widget("jimib.gallery", {
	options : {
		images : []
	},
	_init : function(){
		var self = this;
		var images = wrapObserable( self.options.images || [] );
		
		var p = self.p = new paper.PaperScope();
		
		var views = self.views = [];
		p.setup( self.element.get( 0 ) );
		
		images.subscribe( function( changes ){
			_.each( changes, function( change ){
				switch( change.status ){
					case "added":
						self.addImage( change.value );
					break;
					case "removed":
						self.removeImage( change.value );
					break;
				}
			});
		}, null, "arrayChange" );

		function update(){
			
			var angle = 0.001 * getTime();
			var center = p.view.center;
			
			_.each( self.views, function( view, index ){
				view.update();
			});
			
			p.view.draw();
			requestAnimationFrame( update );
		}
		
		update();
	},
	addImage : function( image ){
		var self = this,
			p = self.p;
			
		p.activate();
		
		var viewImage = createImageView( p, image );
		viewImage.position = new p.Point(300,300);
		viewImage.visible = false;
		viewImage.position = new p.Point(0,-500);
		
		viewImage.on("ready", function(){
			viewImage.visible = true;
			self.views.push( viewImage );
			self._rearrangeImages();
		});
	},
	removeImage : function( image ){
		var self = this;
		self._removeView( self._getViewForModel( image ) );
		self._rearrangeImages();
	},
	_rearrangeImages : function(){
		var self = this,
			p = self.p;
			
		_.each( self.views, function( view, index ){
			index = self.views.length - index - 1;
			console.log( index );
			view.setPosition( new p.Point(
				p.view.center.x + ( index % 2 - 0.5 ) * 500,
				( Math.floor( index / 2 ) + 0.5 ) * 300
			));
		} )
	},
	_removeView : function( view ){
		var self = this;
		if( view ){
			view.remove();
			self.views = _.without( self.views, view );
		}
	},
	_getViewForModel : function( model ){
		var self = this;
		return _.find( self.views, function( viewImage ){
			return viewImage.model == model ? true : false;
		} );
	}
});

function createImageView( paper, model ){
	showLoader();
	var image = new paper.Raster( model.sizes.medium );
	image.pivot = new paper.Point( 0, 0 );
	image.position = new paper.Point( 0, 0 );
	
	var rect = new paper.Shape.Rectangle(new paper.Rectangle(0,0,100,100));
	rect.pivot = new paper.Point( 0, 0 );
	rect.position = new paper.Point( 0, 0 );
		
	rect.fillColor = "#ffffff";
	
	var group = new paper.Group( rect, image );
	group.transformContent = false;
	group.pivot = new paper.Point( 0, 0 );
	
	group.model = model;
	group.type = "image";
	
	var positionTarget = new paper.Point(0,0);
	group.setPosition = function( position ){
		positionTarget = position;
	}
	
	group.update = function(){
		if( !isHeld ){
			var r = 0.9,
			ir = 1 - r;
			
			group.position = new paper.Point( 
				r * group.position.x + ir * positionTarget.x,
				r * group.position.y + ir * positionTarget.y
			);
		}
	}
	
	image.image.onload = function(){
		resize();
		hideLoader();
		group.emit("ready");
	}
	
	group.on("mousedown", onViewHandler );
	group.on("touchstart", onViewHandler );
	
	var evtStart, positionStart, isHeld = false;	
	function onViewHandler( evt ){
		evt = evt.event || evt;
		normaliseEvent( evt );
		evt.preventDefault();
		
		switch( evt.type ){
			case "touchstart":
			case "mousedown":
				evtStart = evt;
				positionStart = group.position;
				isHeld = true;
				
				group.bringToFront();
				$( document ).on("touchmove mousemove touchend touchendfinal touchcancel mouseup", onViewHandler );
			break;
			
			case "touchmove":
			case "mousemove":
				if( evtStart ){
					group.position = new paper.Point(
						positionStart.x + ( evt.pageX - evtStart.pageX ),
						positionStart.y + ( evt.pageY - evtStart.pageY )
					);
				}
			break;
			case "touchend":
			case "touchendfinal":
			case "touchcancel":
			case "mouseup":
				evtStart = positionStart = null;
				isHeld = false;
				$( document ).off("touchmove mousemove touchend touchcancel touchendfinal mouseup", onViewHandler );
			break;
			
		}
	}
	
	function resize(){
		
		var padding = 10;
		var rectW = image.bounds.width + 2 * padding;
		var rectH = image.bounds.height + 2 * padding;
		
		rect.scale(
			rectW / rect.bounds.width,
			rectH / rect.bounds.height
		);
		
		rect.position = new paper.Point( 0, 0 );
	}
	
	resize();
	return group;
}