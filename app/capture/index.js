var express = require("express"),
	exec = require("child_process").exec,
	fs = require("fs"),
	path = require("path");

//start up the server
var app = module.exports = express();
fs.mkdir( path.join( __dirname, "tmp" ), function(){} )

app.get("/", function( req, res, next ){
	getImage( function( err, pathToImage ){
		err = err || ( !pathToImage ? "Missing pathToImage" : null )
		
		if( err ){
			next( err );
		}else{
			res.sendFile( pathToImage );
		}
	} );
});

var requests = null;
var capture = null;
function getImage( cb ){
	//requests = requests || [];
	//requests.push( cb );
	
	//if( !capture ){
		var nameOfImage = "photo-" + ( new Date().getTime() ) + ".jpg";
		var pathToImage = path.join( __dirname, "tmp", nameOfImage );
		
		var capture = exec('gphoto2 --capture-image-and-download --filename='+pathToImage,
			function (error, stdout, stderr) {}
		);
		
		function onComplete(){
			fs.exists( pathToImage, function( boolExists ){
				var err = !boolExists ? "Image not created" : null;
				
				cb( err, !err ? pathToImage : null );
		
				setTimeout( function(){
					fs.unlink( pathToImage, function(){} );
				}, 10000 );
			} );
		}
	
		capture.on("exit", onComplete);
		//also provide a timeout if whatever reason our capture doesn't exit
		setTimeout( onComplete, 10000 );
	//}
}