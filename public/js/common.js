$(document).on("touchend", function(){
	$(document).trigger( "touchendfinal" );
});

function getTime(){
	return new Date().getTime();
}

function normaliseEvent( evt, element ){
	if( (!evt.pageX || !evt.pageY) && evt.originalEvent && evt.originalEvent.touches ){
		evt.pageX = evt.originalEvent.touches[0].pageX;
		evt.pageY = evt.originalEvent.touches[0].pageY;
	}
	
	if( element ){
		var offset = $( element ).offset();
		evt.localX = evt.pageX - offset.left;
		evt.localY = evt.pageY - offset.top;
	}
}

function wrapObserable( val ){
	return ko.isObservable( val ) ? val : ko.observable( val );
}

function wrapObserableArray( val ){
	return ko.isObservable( val ) ? val : ko.observableArray( val );
}

function unwrapObserable( val ){
	while( ko.isObservable( val ) ){
		val = ko.unwrap( val );
	}
	return val;
}

if( window.ko ){
	ko.bindingHandlers.widget = {
		init : function( element, valueAccessor, allBindings, viewModel, bindingContext ){
		
			var oElement = element;
			//reach up to the parent pinpad and get the options from it
			var widgets = valueAccessor();
			var $element = $(element);
			//if this element is called component - then we reach up to our parent
			if( $element.prop("tagName").toLowerCase() == "component" ){
				$element = $element.parent();
				element = $element.get(0);
			}
		
			//build up a list of widgets we can access
			element.widget = {};
		
			for( var id in widgets ){
				(function(id){
					var options = widgets[ id ];
					var optionsCustom = {};
			
					//read the custom parameters the user passed through
					try{
						eval( "optionsCustom = {" +$element.attr("options") + "}" );
					}catch(err){
						console.log("Unable to parse parent options", err);
					}
		
					//merge these values
					for( var property in optionsCustom ){
						options[property] = optionsCustom[property];
					}
			
					//add the model to the options
					options.viewModel = viewModel;
			
					//apply the widget
					try{
						$( element )[id]( options );
						element.widget[id] = function(){
							$( element )[id].apply( $( element ), arguments );
						}
					}catch(err){
						console.error("Failed to bind widget '"+id+"'", err);
					}
				})(id);
			}
		
		}
	}

	ko.bindingHandlers.fadeVisible = {
	    init: function(element, valueAccessor) {
	        // Initially set the element to be instantly visible/hidden depending on the value
	        var value = valueAccessor();
			var visible = ko.unwrap(value);
	        $(element).toggle( visible ); // Use "unwrapObservable" so we can handle values that may or may not be observable
			element._opacity = visible ? ($(element).hasClass('backDrop') ? 0.5 : 1 ) : 0;
	    },
	    update: function(element, valueAccessor) {
	        // Whenever the value subsequently changes, slowly fade the element in or out
	        var value = valueAccessor();
	        var opacityTarget = ko.unwrap(value) ? ($(element).hasClass('backDrop') ? 0.5 : 1) : 0;
		
			fadeElement( element, opacityTarget,  Number( $(element).attr("data-fade-rate") || "NaN" ) );
	    }
	};

	ko.bindingHandlers.tapOrClick = {
		'init': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			//check to see if the global flag window.disableTapOrClick is set to true
			function protect( fn ){
				return function(){
					return function( model, evt ){
						if( arguments.length > 0 && !window.disableTapOrClick ){
							valueAccessor()( model, evt );
						}
					}
				}
			}

			ko.bindingHandlers[ "mouseup" ].init(element, protect( valueAccessor ), allBindingsAccessor, viewModel, bindingContext);
			ko.bindingHandlers[ "touchend" ].init(element, protect( valueAccessor ), allBindingsAccessor, viewModel, bindingContext);
		}
	}


	ko.bindingHandlers.touchOrPress = {
		'init': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			ko.bindingHandlers[ "mousedown" ].init(element, valueAccessor,allBindingsAccessor, viewModel, bindingContext);
			ko.bindingHandlers[ "touchstart" ].init(element, valueAccessor,allBindingsAccessor, viewModel, bindingContext);
		}
	}
	
	ko.bindingHandlers.disableTouch = {
		'update' : function( element, valueAccessor ){
			console.log( ko.unwrap( valueAccessor() ), ko.unwrap( valueAccessor() ) ? "none" : "default" );
			setTimeout( function(){
				$(element)[ ko.unwrap( valueAccessor() ) ? "addClass" : "removeClass" ]("disabledTouch");
			},1);
		}
	}
}

//BASE CLASSES
function ComponentModel( params ){
	var self = this;
	//HELPERS FOR CONTROLLING VISIBILITY
	self.isVisible = ko.observable( params.isVisible || false );
	
	self.show = function(){
		self.isVisible( true );
	}
	self.hide = function(){
		self.isVisible( false );
	}
}

//HELPERS
function fadeElement( element, opacity, rate, cbComplete ){
	
	//ensure we have a callback
	cbComplete = cbComplete || function(){};
	
	if( element._opacity == opacity ){
		//no need to do anything
		return cbComplete();
	}
	
	var $el = $(element);
	//get the current opacity
	var opacityStart = element._opacity = isNaN( element._opacity ) ? 1 : element._opacity;
	var opacityEnd = opacity;
	
	rate = rate && !isNaN(rate) ? rate : ( opacityEnd < opacityStart ? 350 : 750);
	
	function updateProgress( progress ){
		var opacity = element._opacity = opacityStart + progress * ( opacityEnd - opacityStart );
		$el.toggle( opacity == 0 ? false : true );
		$el.css("opacity", opacity );
		
		if( progress == 1 ){
			$el.trigger( opacity == 0 ? "fadeOut" : "fadeIn" );
		}
	}
	//animate the element
	$el.stop();
	$el.animate({progress : 1}, {
		progress : function( el, progress ){ updateProgress( progress ); },
		complete : function( el, progress ){ updateProgress( 1 ); cbComplete(); },
		duration : rate * Math.abs( opacityStart - opacity )
	}) 
}