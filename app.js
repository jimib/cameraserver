/* global _config */
/* global GLOBAL */
var express = require("express"),
	path = require("path");
	
GLOBAL._config = require("./config.json");
//start up the server
var app = module.exports = express();

app.set("view engine", "jade");

app.use( require("./app/home") );
app.use( "/capture", require("./app/capture") );
app.use( express.static( path.join(__dirname,"public") ) );

app.listen( _config.port );
